(define-module (srfi srfi-72)
  #:use-module (ice-9 pretty-print)
  #:use-module (system syntax)
  #:export (quasisyntax unsyntax unsyntax-splicing macro-splicing 
                        macro-unsyntax))

#|
--------------------------------------------------------------------------------
This is a partial srfi-72 implementation. The gist of it is captured but there
are a few caveats.

1. #,@ patterns transform the list it is included into ck append macros.
   This has the drawback that less of the visual structure is captured
   and macros who try to match part of that list will most probably fail.

2. syntax-case like macros might be buggy to have inside ... in #,(...)
   We therefor try to throw errors if users make use of the syntax-case
   macrology, try to factor out that into a funciton and call it from #, 
   instead.

3. The #' and #` items found in ... in #,(...) will be interpreted in the light
   of the syntax environment at the #, point in the enclosing quasisyntax 
   expression. Again of one keep on having a functional expression in ... the
   algorithm will work as in a true srfi-72 system.

About the algorithm.

i) We will at an #,code transform into

    (capture-macro
       (old-unsyntax (lambda (x ...) 
                       (with-syntax ((x x) ...) 
                         code))) stx ...)
  
   stx ... is the #' and #` expressions found in code which are replaced with
   a #'x ... in code and instead pushed as arguments to the capture macro
   the capture macro will then capture the syntax environments in of the stx ...
   and push the results as argument the the closure above.

ii)The construction of the #,@ macro depends on analyzing each list. If a
   #,@ is encountered an ck macro c-append will be issued e.g.
   (analyze (a b c #,@(f #'x) d e #,@(h #'y)))
   ->
    (ck () (c-append '(a b c) 
              (c-append (c-capture-macro ...) 
                 (c-appen '(d e) (c-capture-macro ...)))))

   the ck macro is a mechansim to calculate a macro in applicative order
   and is exactly what is neded in order to do splicing. For this we need
   also a modified c-capture-macro see below.

iii) The algorithm is done in two passes, the first passes analyzes according 
     to above and then a second pass with the old quasisyntax behavior (here 
     using the code standard guile uses for quasisyntax and there the 
     old-unsyntax:es is expanded and actual closures are inserted in the 
     resulting syntax expression.

Example:
(let-syntax ((main (lambda (form)
                       
                       (define (make-swap x y)
                         (quasisyntax 
                          (let ((t #,x))
                            (set! #,x #,y)
                            (set! #,y t))))
                       
                       (quasisyntax
                        (let ((s 1)
                              (t 2))
                          #,(make-swap (syntax s) (syntax t))
                          (list s t))))))
  (main)) 
-->
  (2 1)
|#

(define (pp x) (pretty-print (syntax->datum x)) x)

      
(define-syntax-rule (c-syntax-closure s f x ...)
  (let-syntax ((c-capture (lambda (stx)
                            (syntax-case stx (quote)
                              ((_ sss (quote y) (... ...)) 
                               (apply (lambda (ss . z) 
                                        (list #'ck ss 
                                              (list #'quote 
                                                    (apply f z))))
                                      #'(sss y (... ...))))))))
    (c-capture s x ...)))

(define-syntax-rule (syntax-closure f x ...)
  (let-syntax ((capture (lambda (stx)
                          (syntax-case stx ()
                            ((_ y (... ...)) 
                             (apply f #'(y (... ...))))))))
    (capture x ...)))

;; ck macro stubs see

(define-syntax ck-it 
  (lambda (x)
    (syntax-case x (quote)
      ((_ s (quote f))
       (and (identifier? #'f) (eq? (syntax-local-binding #'f) 'macro))
       (call-with-values (lambda () (syntax-local-binding #'f))
         (lambda (m transformer)
           (list #'ck-it #'s (list #'quote (transformer #'f))))))

      ((_ s (quote (quote x))) 
       (list #'ck #'s #'(quote x)))

      ((_ s (quote (f . x)))
       (and (identifier? #'f) (eq? (syntax-local-binding #'f) 'macro))
       (call-with-values (lambda () (syntax-local-binding #'f))
         (lambda (m transformer)
           (list #'ck-it #'s (list #'quote (transformer #'(f . x)))))))
          
      ((_ s f)
       (list #'ck #'s #'f)))))

(define-syntax ck-it/u
  (lambda (x)
    (syntax-case x (quote)
      ((_ s (quote f))
       (and (identifier? #'f) (eq? (syntax-local-binding #'f) 'macro))
       (call-with-values (lambda () (syntax-local-binding #'f))
         (lambda (m transformer)
           (list #'ck-it/u #'s (list #'quote (transformer #'f))))))

      ((_ s (quote (quote x))) 
       (list #'ck #'s #'(quote (x))))

      ((_ s (quote (f . x)))
       (and (identifier? #'f) (eq? (syntax-local-binding #'f) 'macro))
       (call-with-values (lambda () (syntax-local-binding #'f))
         (lambda (m transformer)
           (list #'ck-it/u #'s (list #'quote (transformer #'(f . x)))))))
          
      ((_ s (quote f))
       (list #'ck #'s #'(quote (f)))))))

(define-syntax c-cons
       (syntax-rules (quote)
         ((c-cons s 'h 't) (ck s '(h . t)))))

(define-syntax c-append
       (syntax-rules (quote)
         ((c-append s '() 'l2)       (ck s 'l2))
         ((c-append s '(h . t) 'l2)  (ck s (c-cons 'h (c-append 't 'l2))))
     ))

(define-syntax ck
  (lambda (x)
    (syntax-case x ()
      ((_ . l)
       (let ()
         #'(ck0 . l))))))

(define-syntax ck0
       (syntax-rules (quote)
         ((ck () 'v) v)                      ; yield the value on empty stack
     
         ((ck (((op ...) ea ...) . s) 'v)    ; re-focus on the other argument, ea
           (ck s "arg" (op ... 'v) ea ...))
     
         ((ck s "arg" (op va ...))           ; all arguments are evaluated,
           (op s va ...))                    ; do the redex
     
         ((ck s "arg" (op ...) 'v ea1 ...)   ; optimization when the first ea
           (ck s "arg" (op ... 'v) ea1 ...)) ; was already a value
     
         ((ck s "arg" (op ...) ea ea1 ...)   ; focus on ea, to evaluate it
           (ck (((op ...) ea1 ...) . s) ea))
     
         ((ck s (op ea ...))                 ; Focus: handle an application;
           (ck s "arg" (op) ea ...))         ; check if args are values
     ))

(define-syntax quasisyntax
  (lambda (e)

    ;; Expand returns a list of the form
    ;;    [template[t/e, ...] (replacement ...)]
    ;; Here template[t/e ...] denotes the original template
    ;; with unquoted expressions e replaced by fresh
    ;; variables t, followed by the appropriate ellipses
    ;; if e is also spliced.
    ;; The second part of the return value is the list of
    ;; replacements, each of the form (t e) if e is just
    ;; unquoted, or ((t ...) e) if e is also spliced.
    ;; This will be the list of bindings of the resulting
    ;; with-syntax expression.

    (define (parse x)
      (define g '())
      (define t '())
      
      (define (li-loop x)
        (syntax-case x ()
          ((x . l)
           (cons (loop #'x) (li-loop #'l)))
          (x (loop #'x))))


      (define buggy "srfi-72 unsyntax items with syntax-case like macros is buggy / consider factor out a function")

      (define (loop x)
        (syntax-case x (quote syntax quasisyntax syntax-case syntax-rules 
                              let-syntax with-syntax)
          ((syntax-case . l)
           (error buggy))
          
          ((syntax-rules . l)
           (error buggy))
          
          ((let-syntax . l)
           (error buggy))

          ((with-syntax . l)
           (error buggy))
          
          ((syntax y)
           (let ((sym (datum->syntax x (gensym "template"))))
             (set! g (cons sym g))
             (set! t (cons #'(qsyntax y) t))
             (list #'syntax sym)))

          ((quasisyntax y)
           (let ((sym (datum->syntax x (gensym "template"))))
             (set! g (cons sym g))
             (set! t (cons #'y t))
             (list #'syntax sym)))

          ((_ . _)
           (li-loop x))

          (x #'x)))
      
      (with-syntax ((code       (loop x))
                    ((tt ...)    t)
                    ((gg ...)    g))
        (if (pair? t)
            #'(syntax-closure
               (bunsyntax (lambda (gg ...) 
                        (with-syntax ((gg gg) ...) 
                          code))) 
               tt ...)
            #'(bunsyntax code))))
    
    
    (define (usc-loop x)
      (syntax-case x (unsyntax-splicing)
        (((e0 e ...) r)
         (with-syntax (((nm . nms) (parse #'e0))
                       (tail       (usc-loop #'((e ...) r))))           
           (expand 
            #'(ck () (c-quote (c-append (c-syntax-closure . nms) tail)))
            0)))

        ((() ((unsyntax-splicing e ...) . r))
         (usc-loop #'((e ...) #'r)))

        ((() (u ...))
         #'(u ...))))
      
    (define (appender0 x n)
      (define (breverse l b)
        (let loop ((l l) (r b))
          (if (pair? l)
              (loop (cdr l) (cons (car l) r))
              r)))

      (syntax-case x (unsyntax-splicing unsyntax macro-splicing macro-unsyntax)
        ((() 
          ((macro-splicing e) . r))
         (with-syntax ((e (expand #'e 0)))
           (syntax-case (appender0 #'(() r) 
                                   (+ n 1))
               (quote)
             ((quote ())            
              (if (= n 0)
                  #'e
                  #'(ck-it 'e)))
             (x
              #'(c-append (ck-it 'e) x)))))

        ((() 
          ((macro-unsyntax e) . r))
         (with-syntax ((e (expand #'e 0)))
           (syntax-case (appender0 #'(() r) 
                                   (+ n 1))
               (quote)
             ((quote ())            
              (if (= n 0)
                  #'e
                  #'(ck-it 'e)))
             (x
              #'(c-append (ck-it/u 'e) x)))))

        ((() 
          ((unsyntax-splicing e0 e ...) . r))

         (with-syntax (((nm . nms) (expand (parse #'e0) 0)))
           (syntax-case (appender0 #'(() ((unsyntax-splicing e ...) . r))
                                   (+ n 1))
               (quote)
             ((quote ())
              (if (= n 0)
                  (expand #'(unsyntax e0) 0)
                  #'(c-syntax-closure . nms)))
             (x
              #'(c-append (c-syntax-closure . nms) x)))))

        (((a ...)
          ((macro-splicing e) . r))
         (with-syntax (((a ...)    (reverse #'(a ...)))
                       (e          (expand #'e 0)))
           (syntax-case (appender0 #'(() r) (+ n 1))
               (quote)
             ((quote ())
              #'(c-append '(a ...) (ck-it 'e)))
             (x
              #'(c-append '(a ...)
                  (c-append (ck-it 'e) x))))))

        (((a ...)
          ((macro-unsyntax e) . r))
         (with-syntax (((a ...)    (reverse #'(a ...)))
                       (e          (expand #'e 0)))
           (syntax-case (appender0 #'(() r) (+ n 1))
               (quote)
             ((quote ())
              #'(c-append '(a ...) (ck-it 'e)))
             (x
              #'(c-append '(a ...)
                  (c-append (ck-it/u 'e) x))))))

         (((a ...)
           ((unsyntax-splicing e0 e ...) . r))
          
         (with-syntax (((nm . nms) (expand (parse #'e0) 0))
                       ((a ...)    (reverse #'(a ...))))
           (syntax-case (appender0 #'(() ((unsyntax-splicing e ...) . r)) 
                                   (+ n 1)) 
               (quote)
             ((quote ())
              #'(c-append '(a ...) (c-syntax-closure . nms)))
             (x
              #'(c-append '(a ...)
                          (c-append
                           (c-syntax-closure . nms) x))))))
        
        (((a ...) ((unsyntax-splicing) . r))
         (appender0 #'((a ...) r) n))
        
        (((b ...) (x . l))
         (with-syntax ((x (expand #'x 0)))
           (appender0 #'((x b ...) l) n)))

        (((a ...) b)
         (list #'quote (breverse #'(a ...) (expand #'b 0))))))
    
    (define (appender x)
      (with-syntax ((x x))
        (syntax-case (appender0 #'(() x) 0) (quote)
          ((quote x) #'x)
          (x         #'(ck () x)))))
    
    (define (expand x level)
      (syntax-case x (quasisyntax unsyntax unsyntax-splicing qsyntax quote)
        ((quasisyntax e)
         (with-syntax (((k _)     x) ;; original identifier must be copied
                       (e*  (expand (syntax e) (+ level 1))))
           #'(k e*)))

        ((quote x)
         (with-syntax ((x (expand #'x level)))
           #'(quote x)))

        ((qsyntax x)
         #'x)

        ((unsyntax e)
         (= level 0)
         (expand (parse #'e) 0))
                                              
        ((k . r)
         (and (> level 0)
              (identifier? (syntax k))
              (or (free-identifier=? (syntax k) (syntax unsyntax))
                  (free-identifier=? (syntax k) (syntax unsyntax-splicing))))
         (with-syntax ((r* (expand (syntax r) (- level 1))))
           #'(k . r*)))
        
        ((h . t)
         (appender x))

        (#(e ...)
         (with-syntax (((e* ...)
                        (expand (vector->list (syntax #(e ...))) level)))
           #'(#(e* ...) reps)))
        (other
         #'other)))

    (syntax-case e ()
      ((_ template)
       (with-syntax ((code (expand #'template 0))
                     (qs   (datum->syntax #'cons 'quasisyntax)))
         #'(bquasisyntax code))))))

(define-syntax macro-splicing
  (lambda (e)
    (syntax-violation 'unsyntax "Invalid expression" e)))

(define-syntax macro-unsyntax
  (lambda (e)
    (syntax-violation 'unsyntax "Invalid expression" e)))

(define-syntax unsyntax
  (lambda (e)
    (syntax-violation 'unsyntax "Invalid expression" e)))

(define-syntax qsyntax
  (lambda (e)
    (syntax-violation 'unsyntax "Invalid expression" e)))

(define-syntax unsyntax-splicing
  (lambda (e)
    (syntax-violation 'unsyntax "Invalid expression" e)))

;; This is picked up from ice-9/quasisyntax.scm

;; Quasisyntax in terms of syntax-case.
;;
;; Code taken from and modded from
;; <http://www.het.brown.edu/people/andre/macros/index.html>;
;; Copyright (c) 2006 Andre van Tonder. All Rights Reserved.
;;
;; Permission is hereby granted, free of charge, to any person
;; obtaining a copy of this software and associated documentation
;; files (the "Software"), to deal in the Software without
;; restriction, including without limitation the rights to use, copy,
;; modify, merge, publish, distribute, sublicense, and/or sell copies
;; of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be
;; included in all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
;; BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
;; CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;=========================================================
;;
;; To make nested unquote-splicing behave in a useful way,
;; the R5RS-compatible extension of quasiquote in appendix B
;; of the following paper is here ported to quasisyntax:
;;
;; Alan Bawden - Quasiquotation in Lisp
;; http://citeseer.ist.psu.edu/bawden99quasiquotation.html
;;
;; The algorithm converts a quasisyntax expression to an
;; equivalent with-syntax expression.
;; For example:
;;
;; (quasisyntax (set! #,a #,b))
;;   ==> (with-syntax ((t0 a)
;;                     (t1 b))
;;         (syntax (set! t0 t1)))
;;
;; (quasisyntax (list #,@args))
;;   ==> (with-syntax (((t ...) args))
;;         (syntax (list t ...)))
;;
;; Note that quasisyntax is expanded first, before any
;; ellipses act.  For example:
;;
;; (quasisyntax (f ((b #,a) ...))
;;   ==> (with-syntax ((t a))
;;         (syntax (f ((b t) ...))))
;;
;; so that
;;
;; (let-syntax ((test-ellipses-over-unsyntax
;;               (lambda (e)
;;                 (let ((a (syntax a)))
;;                   (with-syntax (((b ...) (syntax (1 2 3))))
;;                     (quasisyntax
;;                      (quote ((b #,a) ...))))))))
;;   (test-ellipses-over-unsyntax))
;;
;;     ==> ((1 a) (2 a) (3 a))

;; We must use the old version as well to actually splice in code
(define-syntax bquasisyntax
  (lambda (e)

    ;; Expand returns a list of the form
    ;;    [template[t/e, ...] (replacement ...)]
    ;; Here template[t/e ...] denotes the original template
    ;; with unquoted expressions e replaced by fresh
    ;; variables t, followed by the appropriate ellipses
    ;; if e is also spliced.
    ;; The second part of the return value is the list of
    ;; replacements, each of the form (t e) if e is just
    ;; unquoted, or ((t ...) e) if e is also spliced.
    ;; This will be the list of bindings of the resulting
    ;; with-syntax expression.

    (define (bexpand x level)
      (syntax-case x (bquasisyntax bunsyntax bunsyntax-splicing)
        ((bquasisyntax e)
         (with-syntax (((k _)     x) ;; original identifier must be copied
                       ((e* reps) (bexpand (syntax e) (+ level 1))))
           (syntax ((k e*) reps))))
        ((bunsyntax e)
         (= level 0)
         (with-syntax (((t) (generate-temporaries '(t))))
           (syntax (t ((t e))))))
        (((bunsyntax e ...) . r)
         (= level 0)
         (with-syntax (((r* (rep ...)) (bexpand (syntax r) 0))
                       ((t ...)        (generate-temporaries (syntax (e ...)))))
           (syntax ((t ... . r*)
                    ((t e) ... rep ...)))))
        (((bunsyntax-splicing e ...) . r)
         (= level 0)
         (with-syntax (((r* (rep ...)) (bexpand (syntax r) 0))
                       ((t ...)        (generate-temporaries (syntax (e ...)))))
           (with-syntax ((((t ...) ...) (syntax ((t (... ...)) ...))))
             (syntax ((t ... ... . r*)
                      (((t ...) e) ... rep ...))))))
        ((k . r)
         (and (> level 0)
              (identifier? (syntax k))
              (or (free-identifier=? (syntax k) (syntax bunsyntax))
                  (free-identifier=? (syntax k) (syntax bunsyntax-splicing))))
         (with-syntax (((r* reps) (bexpand (syntax r) (- level 1))))
           (syntax ((k . r*) reps))))
        ((h . t)
         (with-syntax (((h* (rep1 ...)) (bexpand (syntax h) level))
                       ((t* (rep2 ...)) (bexpand (syntax t) level)))
           (syntax ((h* . t*)
                    (rep1 ... rep2 ...)))))
        (#(e ...)
         (with-syntax ((((e* ...) reps)
                        (bexpand (vector->list (syntax #(e ...))) level)))
           (syntax (#(e* ...) reps))))
        (other
         (syntax (other ())))))

    (syntax-case e ()
      ((_ template)
       (with-syntax (((template* replacements) (bexpand (syntax template) 0)))
         (syntax
          (with-syntax replacements (syntax template*))))))))

(define-syntax bunsyntax
  (lambda (e)
    (syntax-violation 'unsyntax "Invalid expression" e)))

(define-syntax bunsyntax-splicing
  (lambda (e)
    (syntax-violation 'unsyntax "Invalid expression" e)))
